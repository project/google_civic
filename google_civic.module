<?php

/**
 * @file Google Civic module
 * Responsible for Google Civic Drupal hook implementations.
 */

/**
 * Implements hook_menu().
 */
function google_civic_menu() {

  $items['admin/config/services/google_civic'] = array(
    'title' => 'Google Civic',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_civic_settings_form'),
    'access arguments' => array('administer google civic'),
    'description' => t('API settings'),
    'file' => 'google_civic.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['google_civic/voterinfo'] = array(			       
    'title' => 'Where Do I Vote?',
    'page callback' => 'google_civic_voterinfo',
    'access arguments' => array('access content'),
    'file' => 'google_civic.voterinfo.inc',
    'description' => t('Find your polling place'),
    'type' => MENU_CALLBACK,
  );
  
  $items['wheredoivote'] = $items['google_civic/voterinfo'];
 
  return $items;
}

/**
 * Implements hook_permission().
 */
function google_civic_permission() {
  $perms = array(
    'administer google civic' => array(
      'title' => t('Administer Google Civic'),
    ),
  );

  return $perms;
}

/**
 * Implements hook_block_info().
 */
function google_civic_block_info() {
  $blocks = array();
  $blocks['google_civic_address'] = array(
    'info' => t('Google Civic Address Block'),
    'cache' => DRUPAL_CACHE_PER_USER,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function google_civic_block_view($delta) {
  if ($delta == 'google_civic_address') {
    drupal_add_css(drupal_get_path('module', 'google_civic') .
      '/css/google_civic.css');

    $block['subject'] = t('Find Your Polling Place');
    $block['content'] = drupal_get_form('google_civic_address_form', 'block');
  }
  return $block;
}

/**
 * Implements hook_theme().
 * Styles results
 */
function google_civic_theme($existing, $type, $theme, $path) {
  $tpls_dir = drupal_get_path('module', 'google_civic') . '/tpls';

  $templates = array(
    'google_civic_voterinfo' => array(
      'variables' => array(
        'headline' => NULL,
        'locations' => NULL,
        'early_locations' => NULL,
        'map' => NULL,
        'election_info_link' => NULL,
        'address_form' => NULL,
        'contests' => NULL,
        'sorry' => NULL,
      ),
      'path' => $tpls_dir,
      'template' => 'google-civic-voterinfo',
    ),
    'google_civic_locations' => array(
      'variables' => array(
        'locations' => '',
        'class_prefix' => '',
      ),
      'path' => $tpls_dir,
      'template' => 'google-civic-locations',
    ),
    'google_civic_contest_candidate' => array(
      'variables' => array(
        'title' => '',
        'candidates' => '',
      ),
      'path' => $tpls_dir,
      'template' => 'google-civic-contest-candidate',
    ),
    'google_civic_contest_referendum' => array(
      'variables' => array(
        'title' => '',
        'subtitle' => '',
        'details' => '',
      ),
      'path' => $tpls_dir,
      'template' => 'google-civic-contest-referendum',
    ),
  );

  return $templates;
}

/** Form wrapping */

/**
 * Form builder function for address input form for polling place locator.
 */

function google_civic_address_form($form, &$form_state) {  
  $form = array();

  $form['google_civic_address']['intro'] = array(
    '#type' => 'item',
    '#value' => ('Find voter information for the address below:'),
    '#weight' => 0,
  );

  $address = check_plain(trim($_GET['address']));
  $default_value = $address ? $address : variable_get('google_civic_default_address', '');

  $form['google_civic_address']['address_field'] = array(    
    '#type' => 'textfield',
    '#title' => t('Home Address'),
    '#required' => TRUE,
    '#size' => 35,
    '#attributes' => array('placeholder' => $default_address),
    '#description' => t('Enter your street address to find your polling location'),
    '#default_value' => $default_value,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 100,
  );

  $form['#attributes'] = array(
    'class' => array('google-civic-address-form')
  );

  return $form;
}

function google_civic_address_form_validate($form, &$form_state) {	
  if (empty($form_state['values']['address_field'])) {
    form_set_error('address_field', t('Must provide your address'));
  } 
}

function google_civic_address_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    'google_civic/voterinfo',
    array(
      'query' => array(
        'address' => $form_state['values']['address_field'],
      ),
    ),
  );
}
